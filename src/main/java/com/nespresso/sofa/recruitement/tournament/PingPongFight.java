package com.nespresso.sofa.recruitement.tournament;

public class PingPongFight {

    private Fighter attacker;

    private Fighter defender;

    public PingPongFight(Fighter attacker, Fighter defender) {
        if (attacker == null || defender == null) {
            throw new NullPointerException();
        }
        this.attacker = attacker;
        this.defender = defender;
    }

    public void resolve() {
        while (attacker.isAlive() && defender.isAlive()) {
            Blow blow = attacker.attack();
            defender.defend(blow);
            switchAttacker();
        }
    }

    private void switchAttacker() {
        Fighter previousAttacker = this.attacker;
        attacker = defender;
        defender = previousAttacker;
    }
}
